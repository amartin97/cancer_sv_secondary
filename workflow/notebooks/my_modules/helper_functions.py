# File for storing functions needed in the notebooks


## Imports
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import copy


## Function for plotting quality distribution of ONT reads
def ont_quality_dist_1(file, log = False):

    ### Extraction of file's desired information
    with open(file) as f:
        for line in f:
            if "Number of reads" in line:
                total = int(float("".join(line.split()[-1].split(","))))
                quality_category = ["0-5"]
                quality_number = [0]
            elif "Median read quality" in line:
                median_quality = float(line.split()[-1])
            elif "Mb" in line:
                quality_category.append(line.split()[0].split(":")[0][2:])
                quality_number.append(int(line.split()[1]))

    ### Information modification and merging
    for pos in range(len(quality_category)):
        if pos == 0:
            continue
        elif pos == len(quality_category) - 1:
            quality_category[pos] = ">" + quality_category[pos]
        else:
            if (median_quality > int(quality_category[pos])) and (median_quality < int(quality_category[pos+1])):
                median_quality_pos = pos
            quality_category[pos] = quality_category[pos] + "-" + quality_category[pos+1]
    quality_number[0] = total - quality_number[1]
    for pos in range(0, len(quality_number))[::-1]:
        if pos == (len(quality_number) - 1):
            continue
        elif pos == 0:
            continue
        else:
            quality_number[pos] = quality_number[pos] - sum(quality_number[pos+1:])
    quality_dict = dict(zip(quality_category, quality_number))

    ### Representation
    print("The following plot shows the number of reads distributed by their quality scores.\n" +
          "The red line indicates the median quality score, whose real value is: " + str(median_quality) + ".")
    plt.figure(figsize=(8,7))
    plt.bar(list(quality_dict.keys()), list(quality_dict.values()), width = 0.6, color = "#32a869", log = log)
    plt.title("# de lecturas distribuido por score de calidad", fontsize = 14, fontweight = "bold", y = 1.02)
    plt.xticks(fontsize = 12)
    plt.xlabel("Score", fontweight = "bold", fontsize = 12, labelpad = 12)
    plt.yticks(fontsize = 12)
    if log:
        plt.ylabel("# de lecturas (escala logarítmica)", fontweight = "bold", fontsize = 12, labelpad = 12)
    else:
        plt.ylabel("# de lecturas", fontweight = "bold", fontsize = 12, labelpad = 12)
    plt.axvline(median_quality_pos, linestyle = "--", color = "red")
    plt.show();


## Alternative function for plotting quality distribution of ONT reads
def ont_quality_dist_2(file):

    ### Extraction of file's desired information
    with open(file) as f:
        for line in f:
            if "Number of reads" in line:
                total = int(float("".join(line.split()[-1].split(","))))
                quality_category = ["0"]
                quality_number = [0]
            elif "Median read quality" in line:
                median_quality = float(line.split()[-1])
            elif "Mb" in line:
                quality_category.append(line.split()[0].split(":")[0][1:])
                quality_number.append(int(line.split()[1]))

    ### Information modification and merging
    quality_number[0] = total - quality_number[1]
    for pos in range(0, len(quality_number))[::-1]:
        if pos == (len(quality_number) - 1):
            continue
        elif pos == 0:
            continue
        else:
            quality_number[pos] = quality_number[pos] - sum(quality_number[pos+1:])
    quality_dict = dict(zip(quality_category, quality_number))

    ### Representation
    plt.figure(figsize=(8,7))
    plt.bar(list(quality_dict.keys()), list(quality_dict.values()), width = 1, color = "#32a869",
            align = "edge", edgecolor = "black", linewidth = 1)
    plt.title("Number of reads distributed by quality scores", fontsize = 14, fontweight = "bold", y = 1.02)
    plt.xticks(fontsize = 12)
    plt.xlabel("Quality score", fontweight = "bold", fontsize = 12, labelpad = 12)
    plt.yticks(fontsize = 12)
    plt.ylabel("Number of reads", fontweight = "bold", fontsize = 12, labelpad = 12)
    plt.xticks(range(len(list(quality_dict.keys()))), labels = list(quality_dict.keys()))
    plt.show();


## Function for counting each SV type (it requires a pandas DataFrame created from vcf)
def sv_counter(df):

    ### Information extraction
    variant_info = df.INFO.to_list()
    for pos in range(len(variant_info)):
        fields = variant_info[pos].split(";")
        for field_pos in range(len(fields)):
            if ("TYPE" in fields[field_pos]) and ("TYPE" not in fields[field_pos - 1]):
                variant_info[pos] = fields[field_pos].split("=")[1]

    ### Creation of a dictionary with variants and their number
    variant_type = {}
    for event in variant_info:
        if event not in list(variant_type.keys()):
            variant_type[event] = 1
        else:
            variant_type[event] += 1

    ### Sorting of the dictionary by values (descending)
    variant_type = dict(sorted(variant_type.items(), key = lambda item: item[1], reverse = True))
    return variant_type


## Function for barplotting the number each SV type (it requires the dict created by variant_counter function)
def sv_number_barplot(variant_dict, version, log = False):
    plt.bar(range(len(variant_dict)), height = list(variant_dict.values()), color = "#32a869", log = log)
    plt.xticks(range(len(variant_dict)), labels = list(variant_dict.keys()), fontsize = 12)
    plt.title("# de variantes (GRCh" + str(version) +")", fontsize = 14, fontweight = "bold", y = 1.02)
    plt.xlabel("Tipo de variante", fontweight = "bold", fontsize = 12, labelpad = 12)
    if log:
        plt.ylabel("# de variantes (escala log)", fontweight = "bold", fontsize = 12, labelpad = 12)
        plt.yticks(fontsize = 12)
    else:
        plt.ylabel("# de variantes", fontweight = "bold", fontsize = 12, labelpad = 12)


## Function for stacked-barplotting the number each SV type (it requires the dict created by variant_counter function)
def sv_number_stacked(results, category_names, index = False, index_num = 0, legend = True):

    """
    Parameters
    ----------
    results : dict
        A mapping from question labels to a list of answers per category.
        It is assumed all lists contain the same number of entries and that
        it matches the length of *category_names*.
    category_names : list of str
        The category labels.
    """

    labels = list(results.keys())
    data = np.array(list(results.values()))
    category_colors = plt.get_cmap('inferno')(
        np.linspace(0.15, 0.85, data.shape[1]))
    if index:
        data = np.array([list(results.values())[0][index_num:]])
        category_colors = category_colors[index_num:]
    data_cum = data.cumsum(axis=1)

    fig, ax = plt.subplots(figsize=(15, 1))
    ax.invert_yaxis()
    ax.xaxis.set_visible(False)
    ax.set_xlim(0, np.sum(data, axis=1).max())

    for i, (colname, color) in enumerate(zip(category_names[index_num:], category_colors)):
        widths = data[:, i]
        starts = data_cum[:, i] - widths
        ax.barh(labels, widths, left=starts, height=0.5,
                label=colname, color=color)
        xcenters = starts + widths / 2

        r, g, b, _ = color
        text_color = 'white' if r * g * b < 0.5 else 'darkgrey'
        for y, (x, c) in enumerate(zip(xcenters, widths)):
            ax.text(x, y, str(float(c)), ha='center', va='center',
                    color=text_color)
    if legend:
        ax.legend(ncol=len(category_names), bbox_to_anchor=(0.475, 1.5), loc='center', fontsize='small')

    return fig, ax


## Function for creating a dict with chromosome's names as keys and their possible identifiers as values
def chrs_ids_extractor(file, unlocalized = False):
    chrs = {}
    for number in range(1, 23):
        chrs["chr" + str(number)] = []
    chrs["chrX"] = []
    chrs["chrY"] = []
    chrs["chrM"] = []
    chrs["chrUn"] = []
    with open(file) as f:
        for line in f:
            if ">" in line:
                if "unplaced" in line:
                    chrs["chrUn"].append(line.split()[0][1:])
                elif "mitochondrion" in line:
                    chrs["chrM"].append(line.split()[0][1:])
                else:
                    if unlocalized:
                        if "unlocalized" in line:
                            chrs["chrUn"].append(line.split()[0][1:])
                        else:
                            chrs["chr"+line.split("chromosome")[1].split()[0].split(",")[0]].append(line.split()[0][1:])
                    else:
                        chrs["chr"+line.split("chromosome")[1].split()[0].split(",")[0]].append(line.split()[0][1:])
    return chrs


## Function for parsing the DataFrame from the ONT vcf and returning a different DataFrame
def RM_ont_df_parser(file, df, unlocalized = False, remove = False):

    ### chrs dict creation (keys: chromosome name, values: possible chromosome identifiers)
    chrs = chrs_ids_extractor(file, unlocalized)

    ### Screening
    variants = []
    for pos in range(len(df)):
        variant = df.iloc[pos].to_list()
        variant_df = [str(variant[0]), str(variant[2])]
        try:
            variant_df.append(int(variant[1]))
            variant_df.append(int(variant_df[2]) + abs(int(variant[7].split("SVLEN=")[1].split(";")[0])))
        except ValueError:
            variant_df.extend([str(variant[1]), "-"])
        variant_df.append(int(variant[7].split("SVLEN=")[1].split(";")[0]))
        variant_df.append(str(variant[7].split("SVTYPE=")[1].split(";")[0]))
        chr_name = variant_df[0].strip().split("STRANDBIAS")[0]
        status = -1
        for chrs_pos in range(len(chrs)):
            for chr_id in list(chrs.values())[chrs_pos]:
                if chr_name in chr_id:
                    status = 0
                    variant_df[0] = list(chrs.keys())[chrs_pos]
                    variants.append(variant_df)
                    break
            if status == 0:
                break
    columns = [df.columns.to_list()[0], df.columns.to_list()[2], "variant_begin", "variant_end", "SV_length", "SV_type"]
    variants = pd.DataFrame(variants, columns = columns)
    if remove:
        variants = variants.iloc[list(variants.CHROM != "chrUn")]
    variants.sort_values(by = [variants.columns[0], variants.columns[2]], inplace = True)
    variants.set_index(variants.columns[1], inplace = True)

    return variants


## DEPRECATED -> Function for parsing the DataFrame from the Illumina vcf and
##               returning a different DataFrame. Deprecated because we want to
##               include the possibility of excluding variants in unplaced or
##               unlocalized scaffolds
def both_df_parser_deprecated(file, df):

    # chrs dict creation (keys: chromosome name, values: possible chromosome identifiers)
    chrs = chrs_ids_extractor(file)

    # Screening
    variants = []
    for pos in range(len(df)):
        variant = df.iloc[pos].to_list()
        variant[1] = int(variant[1])
        chr_name = variant[0]
        for chrs_pos in range(len(chrs)):
            if chr_name in list(chrs.values())[chrs_pos]:
                variant[0] = list(chrs.keys())[chrs_pos]
                variants.append(variant)
                break
    variants = pd.DataFrame(variants, columns = df.columns)
    variants.sort_values(by = [variants.columns[0], variants.columns[1]], inplace = True)

    return variants


## Function for parsing the DataFrame from the Illumina vcf and returning a
## different DataFrame.
def both_df_parser(file, df, unlocalized = False, remove = False):

    # chrs dict creation (keys: chromosome name, values: possible chromosome identifiers)
    chrs = chrs_ids_extractor(file, unlocalized)

    # Screening
    variants = []
    for pos in range(len(df)):
        variant = df.iloc[pos].to_list()
        variant[1] = int(variant[1])
        chr_name = variant[0]
        for chrs_pos in range(len(chrs)):
            if chr_name in list(chrs.values())[chrs_pos]:
                variant[0] = list(chrs.keys())[chrs_pos]
                variants.append(variant)
                break
    variants = pd.DataFrame(variants, columns = df.columns)
    if remove:
        variants = variants.iloc[list(variants.CHROM != "chrUn")]
    variants.sort_values(by = [variants.columns[0], variants.columns[1]], inplace = True)

    return variants


## Function for parsing the DataFrame from the Illumina vcf and returning a different DataFrame (executed after both_df_parser function)
def RM_illumina_df_parser(df, manta = False):
    variants = []
    for pos in range(len(df)):
        variant = df.iloc[pos].to_list()
        variant_df = [str(variant[0]), int(variant[1])]
        variant_df.append(variant_df[1] + abs(len(str(variant[4])) - len(str(variant[3]))))
        variant_df.extend([str(variant[3]), str(variant[4])])
        variant_df.append(len(str(variant[4])) - len(str(variant[3])))
        if manta:
            variant_df.append(variant[7].split("SVTYPE=")[1].split(";")[0])
        else:
            if variant_df[-1] < 0:
                variant_df.append("DEL")
            elif variant_df[-1] > 0:
                variant_df.append("INS")
            else:
                variant_df.append("Other")
        variants.append(variant_df)
    columns = [df.columns.to_list()[0], "variant_begin", "variant_end", df.columns.to_list()[3],
               df.columns.to_list()[4], "SV_length", "SV_type"]
    variants = pd.DataFrame(variants, columns = columns)
    variants.sort_values(by = [variants.columns[0], variants.columns[1]], inplace = True)

    return variants


## Function for parsing the RepeatMasker file and returning a DataFrame
def RM_out_parser(file, rm_hap_alt = False):

    with open(file) as f:
        columns = []
        repeats = []
        for line in f:
            if len(line.strip().split()) == 0:
                continue
            if "SW" in line.strip().split():
                columns.extend(line.strip().split()[0:5])
                columns.extend([line.strip().split()[7]] * 3)
                columns.append("strand")
                columns.extend(line.strip().split()[8:10])
                columns.extend([line.strip().split()[-1]] * 3)
            elif "div." in line.strip().split():
                for pos in range(len(line.strip().split())):
                    if pos == (len(line.strip().split()) - 1):
                        columns.append(line.strip().split()[pos])
                    elif pos > 7:
                        columns[pos + 1] = columns[pos + 1] + "_" + line.strip().split()[pos]
                    else:
                        columns[pos] = columns[pos] + "_" + line.strip().split()[pos]
                columns_df = columns[4:7]
                columns_df.append(columns[10])
                columns_df.append(columns[-1])
            else:
                repeat = line.strip().split()
                if rm_hap_alt:
                    if ("Un" not in repeat[4]) and ("random" not in repeat[4]) and ("hap" not in repeat[4]) and ("alt" not in repeat[4]):
                        repeat_df = [str(repeat[4]).split("_")[0], int(repeat[5]), int(repeat[6]), str(repeat[10]), str(repeat[-1])]
                        repeats.append(repeat_df)
                else:
                    if ("Un" not in repeat[4]) and ("random" not in repeat[4]):
                        repeat_df = [str(repeat[4]).split("_")[0], int(repeat[5]), int(repeat[6]), str(repeat[10]), str(repeat[-1])]
                        repeats.append(repeat_df)

    hg = pd.DataFrame(repeats, columns = columns_df)
    hg.sort_values(by = ["query_sequence", "query_begin"], inplace = True)
    hg.set_index("ID", inplace = True)

    return hg


## DEPRECATED -> Function for creating a dictionary with the IDs of the
##               variants laying in each chromosome. Deprecated because index
##               for repeated regions are not always unique.
def repeat_searcher_deprecated(variants_df, repeats_df):
    rm_vars = {}
    for pos_vcf in range(len(variants_df)):
        var = variants_df.iloc[pos_vcf, :]
        repeat_df = repeats_df.iloc[list((repeats_df.query_sequence == var.CHROM) &
                                         (repeats_df.query_end >= var.variant_begin) &
                                         (repeats_df.query_begin <= var.variant_end))]
        if len(repeat_df) == 0:
            continue
        else:
            if var.CHROM not in list(rm_vars.keys()):
                rm_vars[var.CHROM] = [[var.name, repeat_df.index.to_list()]]
            else:
                rm_vars[var.CHROM].append([var.name, repeat_df.index.to_list()])
    return rm_vars


## Function for creating a dictionary with the IDs of the variants laying in each chromosome
def repeat_searcher(variants_df, repeats_df):
    rm_vars = {}
    for pos_vcf in range(len(variants_df)):
        var = variants_df.iloc[pos_vcf, :]
        repeat_df = repeats_df.iloc[list((repeats_df.query_sequence == var.CHROM) &
                                         (repeats_df.query_end >= var.variant_begin) &
                                         (repeats_df.query_begin <= var.variant_end))]
        if len(repeat_df) == 0:
            continue
        else:
            if var.CHROM not in list(rm_vars.keys()):
                rm_vars[var.CHROM] = [[var.name, repeat_df.loc[:, repeat_df.columns[-1]].to_list()]]
            else:
                rm_vars[var.CHROM].append([var.name, repeat_df.loc[:, repeat_df.columns[-1]].to_list()])
    return rm_vars


## Function for stacked-barplotting the number of variants in chromosome (non)-repeated regions
def variant_repeat_stacked_barplot(total_dict, repeat_dict, tech):
    sv_colors = plt.get_cmap('cividis')(np.linspace(0.1, 0.9, 2))
    plt.figure(figsize = (20, 7))
    for pos in range(len(repeat_dict)):
        if list(repeat_dict.keys())[pos].split("chr")[1] in ["M", "X", "Y"]:
            barplot_total = plt.bar(x = [pos + 1],
                                    height = list(total_dict.values())[pos] -
                                              len(list(repeat_dict.values())[pos]),
                                    color = sv_colors[0],
                                    bottom = len(list(repeat_dict.values())[pos]),
                                    label = "regiones\nno repetitivas")
            barplot_repeatm = plt.bar(x = [pos + 1],
                                      height = len(list(repeat_dict.values())[pos]),
                                      color = sv_colors[1],
                                      label = "regiones\nrepetitivas")
        else:
            barplot_total = plt.bar(x = [int(list(total_dict.keys())[pos].split("chr")[1])],
                                    height = list(total_dict.values())[pos] -
                                              len(list(repeat_dict.values())[pos]),
                                    bottom = len(list(repeat_dict.values())[pos]),
                                    color = sv_colors[0],
                                    label = "regiones\nno repetitivas")
            barplot_repeatm = plt.bar(x = [int(list(repeat_dict.keys())[pos].split("chr")[1])],
                                      height = len(list(repeat_dict.values())[pos]),
                                      color = sv_colors[1],
                                      label = "regiones\nno repetitivas")
    labels = [str(num) for num in range(1, 23)]
    labels.extend(["M", "X", "Y"])
    plt.xlim(0, len(repeat_dict) + 1)
    plt.xticks(range(1, len(repeat_dict) + 1), labels = labels, fontsize = 15)
    plt.xlabel("Cromosoma", fontweight = "bold", fontsize = 15, labelpad = 15)
    plt.yticks(fontsize = 15)
    plt.ylabel("# de variantes", fontweight = "bold", fontsize = 15, labelpad = 15)
    if tech == "nanopore":
        plt.legend(handles = [barplot_total, barplot_repeatm], fontsize = 14, bbox_to_anchor = (1.143, 1.016))
    elif tech == "illumina":
        plt.legend(handles = [barplot_total, barplot_repeatm], fontsize = 14, bbox_to_anchor = (1, 1.016))
    plt.title("# de variantes en regiones (no) repetitivas por cromosoma", fontweight = "bold", y = 1.02, fontsize = 18)
    plt.show();


## Function for stacked-barplotting the percentage of variants in chromosome (non)-repeated regions
def variant_repeat_percentage_stacked_barplot(total_dict, repeat_dict, tech):
    sv_colors = plt.get_cmap('cividis')(np.linspace(0.1, 0.9, 2))
    plt.figure(figsize = (20, 7))
    for pos in range(len(repeat_dict)):
        if list(repeat_dict.keys())[pos].split("chr")[1] in ["M", "X", "Y"]:
            barplot_total = plt.bar(x = [pos + 1],
                                    height = (list(total_dict.values())[pos] -
                                               len(list(repeat_dict.values())[pos])) /
                                                list(total_dict.values())[pos] * 100,
                                    color = sv_colors[0],
                                    bottom = len(list(repeat_dict.values())[pos]) /
                                                list(total_dict.values())[pos] * 100,
                                    label = "regiones\nno repetitivas")
            barplot_repeatm = plt.bar(x = [pos + 1],
                                      height = len(list(repeat_dict.values())[pos]) /
                                                list(total_dict.values())[pos] * 100,
                                      color = sv_colors[1],
                                      label = "regiones\nrepetitivas")
        else:
            barplot_total = plt.bar(x = [int(list(total_dict.keys())[pos].split("chr")[1])],
                                    height = (list(total_dict.values())[pos] -
                                               len(list(repeat_dict.values())[pos])) /
                                                list(total_dict.values())[pos] * 100,
                                    bottom = len(list(repeat_dict.values())[pos]) /
                                                list(total_dict.values())[pos] * 100,
                                    color = sv_colors[0],
                                    label = "regiones\nno repetitivas")
            barplot_repeatm = plt.bar(x = [int(list(repeat_dict.keys())[pos].split("chr")[1])],
                                      height = len(list(repeat_dict.values())[pos]) /
                                                list(total_dict.values())[pos] * 100,
                                      color = sv_colors[1],
                                      label = "regiones\nrepetitivas")
    labels = [str(num) for num in range(1, 23)]
    labels.extend(["M", "X", "Y"])
    plt.xlim(0, len(repeat_dict) + 1)
    plt.xticks(range(1, len(repeat_dict) + 1), labels = labels, fontsize = 15)
    plt.xlabel("Cromosoma", fontweight = "bold", fontsize = 15, labelpad = 15)
    plt.yticks(range(0, 110, 10), [str(num) + "%" for num in range(0, 110, 10)], fontsize = 15)
    plt.ylabel("Porcentaje (%)", fontweight = "bold", fontsize = 15, labelpad = 15)
    plt.legend(handles = [barplot_total, barplot_repeatm], fontsize = 14, bbox_to_anchor = (1, 1.016))
    plt.title("% de variantes en regiones (no) repetitivas por cromosoma",
              fontweight = "bold", y = 1.02, fontsize = 18)
    plt.show();


## Function for stacked-barplotting the percentage of each chromosome formed by (non)-repeated regions
def repeatedchr_percentage_stacked_barplot(repeat_df, genome_file, tech, percentage = False):

    ### Creation of a dict with each chr (key) and its number of bases in repeated regions (value)
    repeat_lengths = {}
    for chrom in list(np.unique(repeat_df.query_sequence.to_list())):
        repeat_lengths[chrom] = sum(list(repeat_df.iloc[list(repeat_df.query_sequence == chrom)].query_end -
                                         repeat_df.iloc[list(repeat_df.query_sequence == chrom)].query_begin))

    ### Creation of a dict with each chr (key) and its total number of bases (value)
    chrs_lengths = {}
    with open(genome_file, "r") as f:
        for line in f:
            if ">" in line:
                if ("unplaced" in line) or ("unlocalized" in line):
                    status = -1
                else:
                    status = 0
                    if "mitochondrion" in line:
                        name = "chrM"
                    else:
                        name = "chr" + line.split()[4].split(",")[0]
                    if name not in list(chrs_lengths.keys()):
                        chrs_lengths[name] = 0
                    else:
                        continue
            else:
                if status == -1:
                    continue
                elif status == 0:
                    chrs_lengths[name] += len(line.strip())
    chrs_lengths = dict(sorted(chrs_lengths.items(), key = lambda item: item[0]))

    ### Stacked bar plot of the % of chr in (non-)repeated regions
    sv_colors = plt.get_cmap('cividis')(np.linspace(0.1, 0.9, 2))
    fig = plt.figure(figsize = (20, 7))
    for pos in range(len(chrs_lengths)):
        if list(chrs_lengths.keys())[pos].split("chr")[1] in ["M", "X", "Y"]:
            if percentage:
                barplot_total = plt.bar(x = [pos + 1],
                                        height = (list(chrs_lengths.values())[pos] - list(repeat_lengths.values())[pos]) /
                                                     list(chrs_lengths.values())[pos] * 100,
                                        color = sv_colors[0],
                                        bottom = list(repeat_lengths.values())[pos] /
                                                     list(chrs_lengths.values())[pos] * 100,
                                        label = "regiones\nno repetitivas")
                barplot_repeatm = plt.bar(x = [pos + 1],
                                          height = list(repeat_lengths.values())[pos] /
                                                       list(chrs_lengths.values())[pos] * 100,
                                          color = sv_colors[1],
                                          label = "regiones\nrepetitivas")
            else:
                barplot_total = plt.bar(x = [pos + 1],
                                        height = list(chrs_lengths.values())[pos] - list(repeat_lengths.values())[pos],
                                        color = sv_colors[0],
                                        bottom = list(repeat_lengths.values())[pos],
                                        label = "regiones\nno repetitivas")
                barplot_repeatm = plt.bar(x = [pos + 1],
                                          height = list(repeat_lengths.values())[pos],
                                          color = sv_colors[1],
                                          label = "regiones\nrepetitivas")
        else:
            if percentage:
                barplot_total = plt.bar(x = [int(list(chrs_lengths.keys())[pos].split("chr")[1])],
                                        height = (list(chrs_lengths.values())[pos] - list(repeat_lengths.values())[pos]) /
                                                     list(chrs_lengths.values())[pos] * 100,
                                        bottom = list(repeat_lengths.values())[pos] /
                                                     list(chrs_lengths.values())[pos] * 100,
                                        color = sv_colors[0],
                                        label = "regiones\nno repetitivas")
                barplot_repeatm = plt.bar(x = [int(list(chrs_lengths.keys())[pos].split("chr")[1])],
                                          height = list(repeat_lengths.values())[pos] /
                                                       list(chrs_lengths.values())[pos] * 100,
                                          color = sv_colors[1],
                                          label = "regiones\nrepetitivas")
            else:
                barplot_total = plt.bar(x = [int(list(chrs_lengths.keys())[pos].split("chr")[1])],
                                        height = list(chrs_lengths.values())[pos] - list(repeat_lengths.values())[pos],
                                        bottom = list(repeat_lengths.values())[pos],
                                        color = sv_colors[0],
                                        label = "regiones\nno repetitivas")
                barplot_repeatm = plt.bar(x = [int(list(chrs_lengths.keys())[pos].split("chr")[1])],
                                          height = list(repeat_lengths.values())[pos],
                                          color = sv_colors[1],
                                          label = "regiones\nrepetitivas")
    labels = [str(num) for num in range(1, 23)]
    labels.extend(["M", "X", "Y"])
    plt.xlim(0, len(chrs_lengths) + 1)
    plt.xticks(range(1, len(chrs_lengths) + 1), labels = labels, fontsize = 15)
    plt.xlabel("Cromosoma", fontweight = "bold", fontsize = 15, labelpad = 15)
    if percentage:
        plt.ylabel("Porcentaje (%)", fontweight = "bold", fontsize = 15, labelpad = 15)
        plt.yticks(range(0, 110, 10), [str(num) + "%" for num in range(0, 110, 10)], fontsize = 15)
        plt.title("% de regiones (no) repetitivas por cromosoma", fontweight = "bold", y = 1.02, fontsize = 18)
        plt.legend(handles = [barplot_total, barplot_repeatm], fontsize = 14, bbox_to_anchor = (1.0025, 1.016))
    else:
        plt.ylabel("Longitud (bases)", fontweight = "bold", fontsize = 15, labelpad = 15)
        plt.yticks(fontsize = 15)
        plt.title("Longitud de regiones (no) repetitivas por cromosoma", fontweight = "bold", y = 1.02, fontsize = 18)
        plt.legend(handles = [barplot_total, barplot_repeatm], fontsize = 14, bbox_to_anchor = (1.14, 1.016))

    return chrs_lengths, repeat_lengths, fig


## Function for plotting the length of each SV
def SVs_length_count_plot(vars_df, tech, zoom = False, zoom_range = 200):
    len_dict = dict(sorted(vars_df.SV_length.value_counts().to_dict().items(), key=lambda item: item[0]))
    plt.figure(figsize = (15, 5))
    if tech == "nanopore":
        if zoom:
            plt.plot(list(len_dict.keys()), list(len_dict.values()), "o", markersize = 3, color = "#32a869")
            plt.xlabel("Longitud (bases)", fontweight = "bold", fontsize = 12, labelpad = 12)
            plt.xlim(-int(zoom_range), int(zoom_range))
        else:
            plt.plot(np.array(list(len_dict.keys())) / 1e6, list(len_dict.values()), "-", color = "#32a869")
            plt.xlabel("Longitud (Megabases)", fontweight = "bold", fontsize = 12, labelpad = 12)
    elif tech == "illumina":
        plt.plot(list(len_dict.keys()), list(len_dict.values()), "o", markersize = 3, color = "#32a869")
        plt.xlabel("Longitud (bases)", fontweight = "bold", fontsize = 12, labelpad = 12)
        if zoom:
            plt.xlim(-50, 50)
    plt.xticks(fontsize = 12)
    plt.ylabel("# de ocurrencias", fontweight = "bold", fontsize = 12, labelpad = 12)
    plt.yticks(fontsize = 12)
    plt.title("# de variantes por longitud", fontweight = "bold", y = 1.02, fontsize = 16)
    plt.show();


## Function for plotting the length of each insertion or deletion
def INS_DEL_length_count_plot(vars_df, tech, zoom = False, zoom_range = [0, 375]):

    ### Creation of a dict with sv_type (key) and its lengths (value)
    type_df = copy.deepcopy(vars_df[(vars_df.SV_type == "INS") |
                                    (vars_df.SV_type == "DEL")])
    type_df.SV_length = np.absolute(type_df.SV_length.to_numpy())
    type_dict = {}
    for sv_type in list(np.unique(type_df.SV_type.values)):
        type_dict[sv_type] = dict(sorted(type_df[type_df.SV_type == sv_type].SV_length.value_counts().to_dict().items(),
                                         key = lambda item: item[0]))

    ### Plot
    sv_colors = plt.get_cmap('inferno')(np.linspace(0.2, 0.8, len(type_dict)))
    for pos in range(len(type_dict.keys())):
        plt.figure(figsize = (20, 7))
        plt.plot(list(type_dict[list(type_dict.keys())[pos]].keys()),
                 list(type_dict[list(type_dict.keys())[pos]].values()),
                 linewidth = 2,
                 color = sv_colors[pos],
                 label = list(type_dict.keys())[pos])
        plt.yscale('log')
        plt.fill_between(list(type_dict[list(type_dict.keys())[pos]].keys()),
                         list(type_dict[list(type_dict.keys())[pos]].values()),
                         color = sv_colors[pos],
                         alpha = 0.5)
        plt.xticks(fontsize = 16)
        if tech == "nanopore":
            if list(type_dict.keys())[pos] == "DEL":
                plt.xlim(-100, max(list(type_dict["INS"].keys())) + 100)
            else:
                plt.xlim(-100, max(list(type_dict["INS"].keys())) + 100)
        if zoom:
            plt.xlim(int(zoom_range[0]), int(zoom_range[1]))
        plt.xlabel("Longitud (bases)", fontweight = "bold", fontsize = 16, labelpad = 14)
        plt.yticks(fontsize = 16)
        plt.ylabel("# de ocurrencias (escala log)", fontweight = "bold", fontsize = 16, labelpad = 14)
        plt.legend(fontsize = 16, bbox_to_anchor = (1.09, 1.0175))
        plt.title("# de " + list(type_dict.keys())[pos] + " por longitud", fontweight = "bold", y = 1.02, fontsize = 18)
        plt.show();

    return type_dict


##
def INS_DEL_profiles(illumina_df, ont_df, not_sep = False, zoom = False, zoom_range = [0, 375]):
    tech_dfs = [illumina_df, ont_df]
    sv_types_dict = {}
    if not_sep:
        tech_dict = {}
        for pos in range(len(tech_dfs)):
            df = copy.deepcopy(tech_dfs[pos][list((tech_dfs[pos].SV_type == "DEL") |
                                                  (tech_dfs[pos].SV_type == "INS"))])
            df.SV_length = np.absolute(df.SV_length.to_list())
            lengths_dict = dict(sorted(df.SV_length.value_counts().to_dict().items(),
                                       key = lambda item: item[0]))
            if pos == 0:
                tech_dict["Illumina"] = lengths_dict
            else:
                tech_dict["ONT"] = lengths_dict
        sv_types_dict["INS/DEL"] = tech_dict
    else:
        for sv_type in ["DEL", "INS"]:
            tech_dict = {}
            for pos in range(len(tech_dfs)):
                df = copy.deepcopy(tech_dfs[pos][(tech_dfs[pos].SV_type == sv_type)])
                if sv_type == "DEL":
                    df.SV_length = np.absolute(df.SV_length.to_list())
                lengths_dict = dict(sorted(df.SV_length.value_counts().to_dict().items(),
                                           key = lambda item: item[0]))
                if pos == 0:
                    tech_dict["Illumina"] = lengths_dict
                else:
                    tech_dict["ONT"] = lengths_dict
            sv_types_dict[sv_type] = tech_dict


    ### Plot
    for pos in range(len(sv_types_dict.keys())):
        plt.figure(figsize = (20, 7))
        for key in list(sv_types_dict[list(sv_types_dict.keys())[pos]].keys()):
            if key == "Illumina":
                sv_color = plt.get_cmap('Spectral')(np.linspace(0.1, 0.91, 2))[1]
                alpha = 1.0
            else:
                sv_color = plt.get_cmap('Spectral')(np.linspace(0.1, 0.91, 2))[0]
                alpha = 0.35
            y = list(sv_types_dict[list(sv_types_dict.keys())[pos]][key].values())
            plt.plot(list(sv_types_dict[list(sv_types_dict.keys())[pos]][key].keys()),
                     y,
                     linewidth = 2,
                     label = key,
                     color = sv_color)
            plt.yscale('log')
            plt.fill_between(list(sv_types_dict[list(sv_types_dict.keys())[pos]][key].keys()),
                             y,
                             alpha = alpha,
                             color = sv_color)
        plt.xticks(fontsize = 16)
        plt.xlabel("Longitud (bases)", fontweight = "bold", fontsize = 16, labelpad = 13)
        plt.yticks(fontsize = 16)
        plt.ylabel("# de ocurrencias (escala log)", fontweight = "bold", fontsize = 16, labelpad = 13)
        plt.legend(fontsize = 16, bbox_to_anchor = (1.17, 1.013))
        if not_sep:
            plt.xlim(-100, max(list(sv_types_dict["INS/DEL"]["ONT"].keys())) + 100)
            plt.title("# de INS y DEL por longitud", fontweight = "bold", y = 1.02,
                      fontsize = 18)
        else:
            plt.xlim(-100, max(list(sv_types_dict[list(sv_types_dict.keys())[1]]["ONT"].keys())) + 100)
            plt.title("# de " + list(sv_types_dict.keys())[pos] + " por longitud", fontweight = "bold", y = 1.02,
                      fontsize = 18)
        if zoom:
            plt.xlim(int(zoom_range[0]), int(zoom_range[1]))
        plt.show();

    return sv_types_dict


##
def INS_DEL_profiles_three(illumina_df, ont_df, manta_df, not_sep = False, zoom = False, zoom_range = [0, 375]):
    tech_dfs = [illumina_df, ont_df, manta_df]
    sv_types_dict = {}
    if not_sep:
        tech_dict = {}
        for pos in range(len(tech_dfs)):
            df = copy.deepcopy(tech_dfs[pos][list((tech_dfs[pos].SV_type == "DEL") |
                                                  (tech_dfs[pos].SV_type == "INS"))])
            df.SV_length = np.absolute(df.SV_length.to_list())
            lengths_dict = dict(sorted(df.SV_length.value_counts().to_dict().items(),
                                       key = lambda item: item[0]))
            if pos == 0:
                tech_dict["Strelka2"] = lengths_dict
            elif pos == 1:
                tech_dict["Sniffles"] = lengths_dict
            else:
                tech_dict["Manta"] = lengths_dict
        sv_types_dict["INS/DEL"] = tech_dict
    else:
        for sv_type in ["DEL", "INS"]:
            tech_dict = {}
            for pos in range(len(tech_dfs)):
                df = copy.deepcopy(tech_dfs[pos][(tech_dfs[pos].SV_type == sv_type)])
                if sv_type == "DEL":
                    df.SV_length = np.absolute(df.SV_length.to_list())
                lengths_dict = dict(sorted(df.SV_length.value_counts().to_dict().items(),
                                           key = lambda item: item[0]))
                if pos == 0:
                    tech_dict["Strelka2"] = lengths_dict
                elif pos == 1:
                    tech_dict["Sniffles"] = lengths_dict
                else:
                    tech_dict["Manta"] = lengths_dict
            sv_types_dict[sv_type] = tech_dict


    ### Plot
    for pos in range(len(sv_types_dict.keys())):
        plt.figure(figsize = (20, 7))
        for key in list(sv_types_dict[list(sv_types_dict.keys())[pos]].keys()):
            if key == "Strelka2":
                sv_color = plt.get_cmap('Spectral')(np.linspace(0.1, 0.91, 2))[1]
                alpha = 1.0
            elif key == "Sniffles":
                sv_color = plt.get_cmap('Spectral')(np.linspace(0.1, 0.91, 2))[0]
                alpha = 0.35
            else:
                sv_color = "#32a869"
                alpha = 0.35
            y = list(sv_types_dict[list(sv_types_dict.keys())[pos]][key].values())
            plt.plot(list(sv_types_dict[list(sv_types_dict.keys())[pos]][key].keys()),
                     y,
                     linewidth = 2,
                     label = key,
                     color = sv_color)
            plt.yscale('log')
            plt.fill_between(list(sv_types_dict[list(sv_types_dict.keys())[pos]][key].keys()),
                             y,
                             alpha = alpha,
                             color = sv_color)
        plt.xticks(fontsize = 16)
        plt.xlabel("Longitud (bases)", fontweight = "bold", fontsize = 16, labelpad = 13)
        plt.yticks(fontsize = 16)
        plt.ylabel("# de ocurrencias (escala log)", fontweight = "bold", fontsize = 16, labelpad = 13)
        plt.legend(fontsize = 16, bbox_to_anchor = (1.17, 1.013))
        if not_sep:
            plt.xlim(-100, max(list(sv_types_dict["INS/DEL"]["Sniffles"].keys())) + 100)
            plt.title("# de INS y DEL por longitud", fontweight = "bold", y = 1.02,
                      fontsize = 18)
        else:
            plt.xlim(-100, max(list(sv_types_dict[list(sv_types_dict.keys())[1]]["Sniffles"].keys())) + 100)
            plt.title("# de " + list(sv_types_dict.keys())[pos] + " por longitud", fontweight = "bold", y = 1.02,
                      fontsize = 18)
        if zoom:
            plt.xlim(int(zoom_range[0]), int(zoom_range[1]))

    return sv_types_dict


##
def INV_DUP_length_count(vars_df, plot = False):

    ### Creation of a dict with sv_type (key) and its lengths (value)
    type_df = copy.deepcopy(vars_df[(vars_df.SV_type == "INV") |
                                    (vars_df.SV_type == "DUP") |
                                    (vars_df.SV_type == "INVDUP")])
    type_df.SV_length = np.absolute(type_df.SV_length.to_numpy())
    type_dict = {}
    for sv_type in list(np.unique(type_df.SV_type.values)):
        type_dict[sv_type] = dict(sorted(type_df[type_df.SV_type == sv_type].SV_length.value_counts().to_dict().items(),
                                         key = lambda item: item[0]))

    ### Plot
    if plot:
        sv_colors = plt.get_cmap('inferno')(np.linspace(0.2, 0.8, len(type_dict)))
        for pos in range(len(type_dict.keys())):
            plt.figure(figsize = (20, 7))
            plt.bar(range(len(list(type_dict[list(type_dict.keys())[pos]].keys()))),
                    list(type_dict[list(type_dict.keys())[pos]].values()),
                    color = sv_colors[pos],
                    width = 0.55,
                    label = list(type_dict.keys())[pos])
            plt.xticks(ticks = range(len(list(type_dict[list(type_dict.keys())[pos]].keys())))[::8],
                       labels = list(type_dict[list(type_dict.keys())[pos]].keys())[::8],
                       rotation = 35, fontsize = 16)
            plt.yticks(fontsize = 16)
            plt.ylabel("# of ocurrences", fontweight = "bold", fontsize = 14, labelpad = 14)
            plt.legend(fontsize = 16, bbox_to_anchor = (1.15, 1.0175))
            if pos == 0:
                plt.title("# of SVs per SV length", fontweight = "bold", y = 1.02, fontsize = 18)
            if pos == 2:
                plt.xlabel("SV length (b)", fontweight = "bold", fontsize = 16, labelpad = 14)
            plt.show();

    for key in list(type_dict.keys()):
        type_dict[key] = list(np.unique(sorted(list(type_dict[key].values()))))
    return type_dict


## DEPRECATED -> Function for looking for each SV of a reference DataFrame in
##               another one. Deprecated because nested loops imply infinite
##               execution times.
def SVs_coincident_counter_deprecated(df_ref, df_target, range_num = False, show = False):

    ### Screening
    count = 0
    found = []
    not_found = []
    for pos in range(len(df_ref)):
        var = df_ref.iloc[pos]
        df = df_target.iloc[list((df_target.CHROM == var.CHROM))]
        status = -1
        for candidate in range(len(df)):
            if range_num == False:
                if (int(df.iloc[candidate].POS) == int(var.POS)) and \
                   (df.iloc[candidate].REF == var.REF) and \
                   (df.iloc[candidate].ALT == var.ALT):
                    status = 0
                    count += 1
                    found.append(var.to_list())
                    break
            else:
                if (int(df.iloc[candidate].POS) in range(int(var.POS) - range_num, int(var.POS) + range_num, 1)):
                    status = 0
                    count += 1
                    found.append(var.to_list())
                    break
        if status == -1:
            not_found.append(var.to_list())
            if show:
                print(var.to_list())
                print()

    ### Results' DataFrame creation
    found = pd.DataFrame(found, columns = df_ref.columns)
    found.sort_values(by = [found.columns[0], found.columns[1]], inplace = True)
    not_found = pd.DataFrame(not_found, columns = df_ref.columns)
    not_found.sort_values(by = [not_found.columns[0], not_found.columns[1]], inplace = True)

    ### Output message
    print("The approach followed in the built workflow has driven to the identification of " + str(count) +
          "\nSVs out of the " + str(len(df_ref)) +
          " identified in the work from which the data was obtained. It means\nrecovering " +
          str(100 * count // len(df_ref)) + "% of the originaly identified SVs.")

    return found, not_found


## Function for looking for each SV of a reference DataFrame in another one.
def SVs_coincident_counter(df_ref, df_target, range_num = False):

    ### Definition of output DataFrames
    found_variants = pd.DataFrame(columns = df_ref.columns)
    not_found_variants = pd.DataFrame(columns = df_ref.columns)

    ### Search of coincident variants
    for pos in range(len(df_ref)):
        var = df_ref.iloc[pos]
        if range_num == False:
            df = df_target.iloc[list((df_target.CHROM == var.CHROM) &
                                     (df_target.POS == var.POS) &
                                     (df_target.REF == var.REF) &
                                     (df_target.ALT == var.ALT))]
        else:
            df = df_target.iloc[list((df_target.CHROM == var.CHROM) &
                                     (df_target.POS.between(var.POS - range_num, var.POS + range_num)))]
        if len(df) > 0:
            found_variants = found_variants.append(var)
        else:
            not_found_variants = not_found_variants.append(var)

    ### Output message
    print("The approach followed in the built workflow has driven to the identification of\n" \
          "{0} SVs out of the {1} identified in the work from which the data was obtained.\n" \
          "It means recovering {2}% of the originaly identified SVs.".format(len(found_variants),
                                                                             len(df_ref),
                                                                             round(100 * len(found_variants) / len(df_ref), 2)))

    return found_variants, not_found_variants


## Function for looking for each SV of a reference DataFrame in other two.
def SVs_coincident_counter_three(df_ref, df_target_0, df_target_1):

    ### Definition of output DataFrames
    found_variants_0 = pd.DataFrame(columns = df_ref.columns)
    found_variants_1 = pd.DataFrame(columns = df_ref.columns)
    found_variants_together = pd.DataFrame(columns = df_ref.columns)

    ### Search of coincident variants
    for pos in range(len(df_ref)):
        var = df_ref.iloc[pos]
        df_0 = df_target_0.iloc[list((df_target_0.CHROM == var.CHROM) &
                                     (df_target_0.POS == var.POS) &
                                     (df_target_0.REF == var.REF) &
                                     (df_target_0.ALT == var.ALT))]
        df_1 = df_target_1.iloc[list((df_target_1.CHROM == var.CHROM) &
                                     (df_target_1.POS == var.POS) &
                                     (df_target_1.REF == var.REF) &
                                     (df_target_1.ALT == var.ALT))]
        if len(df_0) > 0:
            found_variants_0 = found_variants_0.append(var)
        if len(df_1) > 0:
            found_variants_1 = found_variants_1.append(var)
        if (len(df_0) > 0) and (len(df_1) > 0):
            found_variants_together = found_variants_together.append(var)

    ### Output message
    print("The approach followed in the built workflow has driven to the identification of\n" \
          "{0} and {1} SVs, respectively for the first and the second given DataFrames,\n" \
          "out of the {2} identified in the work from which the data was obtained.\n" \
          "Moreover the number of same SVs found in both given DataFrames is {3}.".format(len(found_variants_0),
                                                                                          len(found_variants_1),
                                                                                          len(df_ref),
                                                                                          len(found_variants_together)))

    return found_variants_0, found_variants_1, found_variants_together
