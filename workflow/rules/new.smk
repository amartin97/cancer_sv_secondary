# NANOGLADIATOR

rulename = "bigWig_creation_indexing"
rule bigWig_creation_indexing:
    input:
        lambda wildcards: config["genome"][wildcards.version]
    output:
        fasta = f"{outdir}/genome/GEM/{{version}}.fna",
        index = f"{outdir}/genome/GEM/{{version}}.gem"
    conda:
        "../envs/gem.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        f"{logdir}/genome/GEM/execution/{{version}}.gem.log"
    benchmark:
        f"{logdir}/genome/GEM/execution/{{version}}.gem.bmk"
    shell:
        """
        mkdir -p {outdir}/genome/GEM
        gunzip -c {input} > {output.fasta} 2> {log}
        gem-indexer -T {threads} -i {output.fasta} -o {outdir}/genome/GEM/{wildcards.version} --verbose 2>> {log}
        """

rulename = "bigWig_creation_mappability"
rule bigWig_creation_mappability:
    input:
        f"{outdir}/genome/GEM/{{version}}.gem"
    output:
        f"{outdir}/genome/GEM/{{version}}_100.mappability"
    conda:
        "../envs/gem.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/genome/GEM/execution/{{version}}_100.mappability.out",
        err = f"{logdir}/genome/GEM/execution/{{version}}_100.mappability.err"
    benchmark:
        f"{logdir}/genome/GEM/execution/{{version}}_100.mappability.bmk"
    shell:
        "gem-mappability -T {threads} -I {input} -o {outdir}/genome/GEM/{wildcards.version}_100 -l 100 -m 2 > {log.out} 2> {log.err}"

rulename = "bigWig_creation_bedgraph"
rule bigWig_creation_bedgraph:
    input:
        index = f"{outdir}/genome/GEM/{{version}}.gem",
        mapp = f"{outdir}/genome/GEM/{{version}}_100.mappability"
    output:
        tmp = f"{outdir}/genome/GEM/{{version}}_100_tmp.bg",
        bg = f"{outdir}/genome/GEM/{{version}}_100.bg"
    conda:
        "../envs/gem.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        f"{logdir}/genome/GEM/execution/{{version}}_100.bg.log"
    benchmark:
        f"{logdir}/genome/GEM/execution/{{version}}_100.bg.bmk"
    shell:
        """
        gem-2-bed mappability -I {input.index} -i {input.mapp} -o {outdir}/genome/GEM/{wildcards.version}_100_tmp 2> {log}
        awk -v OFS="\t" '{{print $1, $(NF - 2), $(NF - 1), $NF}}' {output.tmp} > {output.bg} 2>> {log}
        bedSort {output.bg} {output.bg} 2>> {log}
        """

rulename = "bigWig_creation_sizes"
rule bigWig_creation_sizes:
    input:
        f"{outdir}/genome/GEM/{{version}}.fna"
    output:
        fai = f"{outdir}/genome/GEM/{{version}}.fna.fai",
        sizes = f"{outdir}/genome/GEM/{{version}}.sizes"
    conda:
        "../envs/samtools.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        f"{logdir}/genome/GEM/execution/{{version}}.sizes.log"
    benchmark:
        f"{logdir}/genome/GEM/execution/{{version}}.sizes.bmk"
    shell:
        """
        samtools faidx {input} -o {output.fai} 2> {log}
        cut -f1,2 {output.fai} > {output.sizes} 2>> {log}
        """

rulename = "bigWig_creation_bigwig"
rule bigWig_creation_bigwig:
    input:
        f"{outdir}/genome/GEM/{{version}}_100.bg",
        f"{outdir}/genome/GEM/{{version}}.sizes"
    output:
        f"{outdir}/genome/GEM/{{version}}_100.bw"
    conda:
        "../envs/gem.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/genome/GEM/execution/{{version}}_100.bw.out",
        err = f"{logdir}/genome/GEM/execution/{{version}}_100.bw.err"
    benchmark:
        f"{logdir}/genome/GEM/execution/{{version}}_100.bw.bmk"
    shell:
        "bedGraphToBigWig {input} {output} > {log.out} 2> {log.err}"

rulename = "nanogladiator_source_target"
rule nanogladiator_source_target_hg19:
    input:
        fasta = f"{outdir}/genome/GEM/reference_genome_37.fna",
        bw = f"{outdir}/NanoGLADIATOR_1.0/data/ucsc.hg19.bw"
    output:
        f"{outdir}/nanopore/nanogladiator/nanopore_37_SourceTarget.txt"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        f"{logdir}/nanopore/nanogladiator/execution/nanopore_37_SourceTarget.txt.log"
    benchmark:
        f"{logdir}/nanopore/nanogladiator/execution/nanopore_37_SourceTarget.txt.bmk"
    shell:
        "echo {input.bw} {input.fasta} > {output} 2> {log}"

rulename = "nanogladiator_source_target"
rule nanogladiator_source_target_hg38:
    input:
        fasta = f"{outdir}/genome/GEM/reference_genome_38.fna",
        bw = f"{outdir}/NanoGLADIATOR_1.0/data/GCA_000001405.15_GRCh38.bw"
    output:
        f"{outdir}/nanopore/nanogladiator/nanopore_38_SourceTarget.txt"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        f"{logdir}/nanopore/nanogladiator/execution/nanopore_38_SourceTarget.txt.log"
    benchmark:
        f"{logdir}/nanopore/nanogladiator/execution/nanopore_38_SourceTarget.txt.bmk"
    shell:
        "echo {input.bw} {input.fasta} > {output} 2> {log}"

rulename = "nanogladiator_initialize"
rule nanogladiator_initialize_hg19:
    input:
        f"{outdir}/nanopore/nanogladiator/nanopore_37_SourceTarget.txt"
    output:
        directory(f"{outdir}/NanoGLADIATOR_1.0/data/targets/hg19/nanopore_37")
    conda:
        "../envs/nanogladiator.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/nanopore/nanogladiator/execution/nanogladiator_initialize_37.out",
        err = f"{logdir}/nanopore/nanogladiator/execution/nanogladiator_initialize_37.err"
    benchmark:
        f"{logdir}/nanopore/nanogladiator/execution/nanogladiator_initialize_37.bmk"
    shell:
        """
        cd {outdir}/NanoGLADIATOR_1.0
        perl ReferenceWindowInitialize.pl -M genome \
                                          -S {input} \
                                          -T nanopore_37 \
                                          -W 10000 \
                                          -A hg19 \
                                          > {log.out} 2> {log.err}
        cd -
        """

rulename = "nanogladiator_initialize"
rule nanogladiator_initialize_hg38:
    input:
        f"{outdir}/nanopore/nanogladiator/nanopore_38_SourceTarget.txt"
    output:
        directory(f"{outdir}/NanoGLADIATOR_1.0/data/targets/hg38/nanopore_38")
    conda:
        "../envs/nanogladiator.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/nanopore/nanogladiator/execution/nanogladiator_initialize_38.out",
        err = f"{logdir}/nanopore/nanogladiator/execution/nanogladiator_initialize_38.err"
    benchmark:
        f"{logdir}/nanopore/nanogladiator/execution/nanogladiator_initialize_38.bmk"
    shell:
        """
        cd {outdir}/NanoGLADIATOR_1.0
        perl ReferenceWindowInitialize.pl -M genome \
                                          -S {input} \
                                          -T nanopore_38 \
                                          -W 10000 \
                                          -A hg38 \
                                          > {log.out} 2> {log.err}
        cd -
        """

rulename = "nanogladiator_prepare"
rule nanogladiator_prepare:
    input:
        f"{outdir}/nanopore/sorted_bam/nanopore_{{sample}}_{{version}}_sorted.bam"
    output:
        f"{outdir}/nanopore/nanogladiator/nanopore_{{sample}}_{{version}}_NGPrepare.txt"
    conda:
        "../envs/nanogladiator.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/nanopore/nanogladiator/execution/nanogladiator_prepare_{{sample}}_{{version}}.out",
        err = f"{logdir}/nanopore/nanogladiator/execution/nanogladiator_prepare_{{sample}}_{{version}}.err",
    benchmark:
        f"{logdir}/nanopore/nanogladiator/execution/nanogladiator_prepare_{{sample}}_{{version}}.bmk"
    shell:
        """
        cd {outdir}/NanoGLADIATOR_1.0
        echo {input} {outdir}/nanopore/nanogladiator/nanopore_{wildcards.sample}_{wildcards.version}/prepare {wildcards.sample} > {output}
        perl NanoGLADIATORPrepare.pl {output} \
                                     --processors {threads} \
                                     --target nanopore_{wildcards.version} \
                                     --assembly hg19 \
                                     > {log.out} 2> {log.err}
        cd -
        """

rulename = "nanogladiator_analysis"
rule nanogladiator_analysis:
    input:
        f"{outdir}/nanopore/nanogladiator/nanopore_{{sample}}_{{version}}_NGPrepare.txt"
    output:
        txt = f"{outdir}/nanopore/nanogladiator/nanopore_{{sample}}_{{version}}_NGAnalysis.txt",
        dir = directory(f"{outdir}/nanopore/nanogladiator/nanopore_{{sample}}_{{version}}/")
    conda:
        "../envs/nanogladiator.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/nanopore/nanogladiator/execution/nanogladiator_analysis_{{sample}}_{{version}}.out",
        err = f"{logdir}/nanopore/nanogladiator/execution/nanogladiator_analysis_{{sample}}_{{version}}.err"
    benchmark:
        f"{logdir}/nanopore/nanogladiator/execution/nanogladiator_analysis_{{sample}}_{{version}}.bmk"
    shell:
        """
        cd {outdir}/NanoGLADIATOR_1.0
        awk '{{print "T"NR, $2, $3}}' {input} > {output.txt}
        perl NanoGLADIATORAnalysis.pl {output.txt} \
                                      --processors {threads} \
                                      --target nanopore_{wildcards.version} \
                                      --assembly hg19 \
                                      --output $(dirname {output.txt})/nanopore_{wildcards.sample}_{wildcards.version} \
                                      --mode nocontrol \
                                      > {log.out} 2> {log.err}
        cd -
        """

################################################################################

# rule clean:
#     input:
#         "../sorted_bam/"
#     shell:
#         """
#         if [-e $(find ../ -name "illumina*")]; then
#             rm -rf {input}"
#         fi
#         """

################################################################################

# Illumina: Structural variants annotation
rulename = "illumina_SVs_annotation"
rule illumina_SVs_annotation:
    input:
        f"{outdir}/illumina/sv_calling/strelka/GRCh37_{{aligner}}/results/variants/somatic.indels.vcf.gz"
    output:
        f"{outdir}/illumina/sv_annotation/GRCh37_{{aligner}}/snpEff_genes.txt",
        f"{outdir}/illumina/sv_annotation/GRCh37_{{aligner}}/snpEff_summary.html"
    conda:
        "../envs/snpeff.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/illumina/sv_annotation/execution/snpEff_GRCh37_{{aligner}}.out",
        err = f"{logdir}/illumina/sv_annotation/execution/snpEff_GRCh37_{{aligner}}.err"
    benchmark:
        f"{logdir}/illumina/sv_annotation/execution/snpEff_GRCh37_{{aligner}}.bmk"
    shell:
        """
        mkdir -p $(dirname {output})
        cd $(dirname {output})
        snpEff -v -t {threads} GRCh37.p13.RefSeq {input} > {log.out} 2> {log.err}
        """


# ONT: SV calling
rulename = "sniffles_sv_calling_10"
rule sniffles_sv_calling_10:
    input:
        f"{outdir}/nanopore/sorted_bam/nanopore_{{sample}}_{{version}}_sorted.bam"
    output:
        f"{outdir}/nanopore/sniffles_res/nanopore_{{sample}}_{{version}}_10_SV_calling.vcf"
    conda:
        "../envs/sniffles.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:
        out = f"{logdir}/nanopore/sniffles_res/execution/nanopore_{{sample}}_{{version}}_10_SV_calling.vcf.out",
        err = f"{logdir}/nanopore/sniffles_res/execution/nanopore_{{sample}}_{{version}}_10_SV_calling.vcf.err"
    benchmark:
        f"{logdir}/nanopore/sniffles_res/execution/nanopore_{{sample}}_{{version}}_10_SV_calling.vcf.bmk"
    shell:
        "sniffles -m {input} -v {output} --threads {threads} --min_length 10 > {log.out} 2> {log.err}"


# Illumina: SV calling
rulename = "delly_sv_calling"
rule delly_sv_calling:
    input:
        genome = f"{outdir}/genome/bwa/reference_genome_{{version}}.fna",

    output:

    conda:
        "../envs/delly.yaml"
    resources:
        mem_mb = get_resource(rulename, "mem_mb"),
        walltime = get_resource(rulename, "walltime")
    threads:
        get_resource(rulename, "threads")
    log:

    benchmark:

    shell:
        "delly call -g {input.genome} -x "
