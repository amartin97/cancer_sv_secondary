rule illumina_pickle_creation:
    input:
        f"{outdir}"
    output:
        protected(f"{outdir}/illumina/sv_calling/strelka/GRCh37_bwa/RM_bwa_37.pkl"),
        protected(f"{outdir}/illumina/sv_calling/strelka/GRCh37_bwa/RM_bwa_37_f.pkl")
    conda:
        "../envs/jupyter.yaml"
    notebook:
        "../notebooks/illumina_vcf.py.ipynb"

rule ont_pickle_creation:
    input:
        f"{outdir}"
    output:
        protected(f"{outdir}/nanopore/sniffles_res/RM_37.pkl")
    conda:
        "../envs/jupyter.yaml"
    notebook:
        "../notebooks/nanopore_vcf.py.ipynb"

rule ont_10_pickle_creation:
    input:
        f"{outdir}"
    output:
        protected(f"{outdir}/nanopore/sniffles_res/RM_37_10.pkl"),
        protected(f"{outdir}/nanopore/sniffles_res/found_genes_10.pkl")
    conda:
        "../envs/jupyter.yaml"
    notebook:
        "../notebooks/nanopore_vcf_10.py.ipynb"
