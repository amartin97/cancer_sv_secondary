# cancer_sv_secondary

## Introduction

cancer_sv_secondary is a repository that stores the analysis of genetic variants from short and long read sequencing data of a melanoma cancer cell line (COLO829) and its normal counterpant (COLO829BL).

## Dependencies

- [Jupyter Notebook](https://github.com/jupyter)
- [NumPy](https://github.com/numpy/numpy)
- [Pandas](https://github.com/pandas-dev/pandas)
- [Matplotlib](https://github.com/matplotlib/matplotlib)

## Author

[Alejandro Martín Muñoz](https://gitlab.com/amartin97)

## Contributors

[Tomás Di Domenico](https://gitlab.com/tdido)
